#!/usr/bin/env python
import SocketServer
import sys
import os
import logging
from subprocess import call

class BrightnessServer(SocketServer.BaseRequestHandler):
  
  def handle(self):
    # change these to your locations. both paths can be pretty much anywhere as long as they're correct and consistent.
    LOGPATH = '/home/hlmtre/brightness.log'
    SCRIPTPATH = '/usr/local/bin/brightness'

    self.logger = logging.getLogger('brightness')
    self.logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler(LOGPATH)
    fh.setLevel(logging.DEBUG)
    frmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(frmt)
    self.logger.addHandler(fh)

    self.script = SCRIPTPATH
    self.data = self.request.recv(1024).strip()

    if self.data in ['up','down','min','max']:
      try:
        retval = call([self.script, self.data], stderr=None)
        self.request.sendall(str(retval))
        self.logger.info("successfully set brightness " + self.data)
      except:
        self.logger.warning("failed to set brightness. " + sys.exc_info()[0]) 
        pass

def main():
  pid = os.fork()
  if pid == 0:
    HOST = 'localhost'
    PORT = 32768
    server = SocketServer.TCPServer((HOST, PORT), BrightnessServer)
    server.serve_forever()
  else:
    sys.exit(0)

if __name__ == "__main__":
  main()
