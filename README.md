to start up automatically, add executing the server.py to your rc.local (or whatever script runs at startup)
replace paths with your own locations.

for your /etc/rc.local file:
`/usr/bin/env python /home/hlmtre/src/brightness/server.py`

for your i3 config: (replace path, obv)

     #brightness 
     bindsym XF86MonBrightnessDown exec python /home/hlmtre/src/brightness/client.py down
     bindsym XF86MonBrightnessUp exec python /home/hlmtre/src/brightness/client.py up

the shell script must be run as root, as well as the server.py. the client can run as a regular unprivileged user.
